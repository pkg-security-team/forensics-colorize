Source: forensics-colorize
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: https://github.com/jessek/colorize
Vcs-Git: https://salsa.debian.org/pkg-security-team/forensics-colorize.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/forensics-colorize

Package: forensics-colorize
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: sxiv
Description: show differences between files using color graphics
 forensics-colorize is a set of tools to visually compare large files, as
 filesystem images, creating graphics of them. It is intuitive because the
 produced graphics provide a quick and perfect sense about the percentage
 of changes between two files.
 .
 Comparing large textual files using a simple diff can produce a very big
 result in lines, causing confusion. On the other hand, diff is improper
 to compare binary files.
 .
 This package provides two command line programs: filecompare and colorize.
 The filecompare command is used to create a special and auxiliary input
 file for colorize. The colorize command will generate an intuitive graphic
 that will make easier to perceive the level of changes between the files.
